#!/usr/bin/perl
# ToDo:
# Network managing
# Prunes

use strict;
use warnings;

# Global Variables
my $scriptname  = $0;
my $argv1       = $ARGV[0];
my $argv2       = $ARGV[1];
my $argv3       = $ARGV[2];

# Prechecks
if(`which docker` eq '')
{
    clear();
    printf("#\n#\n");
    printf("# ERROR!\n");
    printf("# Docker isn't installed!\n");
    printf("# Check with: \`which docker\`\n");
    printf("# Read the docs: https://docs.docker.com/install/");
    printf("#\n#\n");
    exit;
}

if(`ps -elf | grep docker | grep -v grep` eq '')
{
    clear();
    printf("#\n#\n");
    printf("# ERROR!\n");
    printf("# Docker isn't running!\n");
    printf("# Check with: \`ps -elf | grep docker | grep -v grep\`\n");
    printf("#\n#\n");
    exit;
}


# Main
if(!@ARGV)
{
    printhelp();
    exit;
}

if($argv1 eq 'images' || $argv1 eq 'i')
{
    if(!length($argv2)){ printf("# You must provide parameters!\n# \"$scriptname help\" for help"); exit; }
    if($argv2 eq 'show' || $argv2 eq 's')
    {
        system("docker images -a");
        exit;
    }
    elsif($argv2 eq 'del' || $argv2 eq 'd')
    {
        if(!length($argv3)){ printf("# You must provide parameters!\n# \"$scriptname help\" for help"); exit; }
        if($argv3 eq 'all' || $argv3 eq 'a')
        {
            clear();
            printf("# Do you want to delete all images?\n\n");
            system("docker images -a");
            printf("\n\n# (y/N): ");
            my $yN = <STDIN>;
            chomp($yN);
            if($yN eq 'y' || $yN eq 'Y')
            {
                system("docker rmi -f \$(docker images -a | awk '{print \$3}')");
                printf("#\n#\n# Deleted successfully!\n#\n#");
            }
            else
            {
                printf("#\n#\n# Abort mission!\n#\n#");
            }
            exit;
        }
        elsif($argv3 eq 'byname' || $argv3 eq 'bn')
        {
            clear();
            system("docker images -a");
            printf("\n\n");
            printf("# Which images (by name) do you want to delete?\nImage: ");
            my $choose = <STDIN>;
            chomp($choose);
            clear();
            printf("# Do you want to delete the following images?\n\n");
            system("docker images -a | grep $choose");
            printf("\n\n# (y/N): ");
            my $yN = <STDIN>;
            chomp($yN);
            if($yN eq 'y' || $yN eq 'Y')
            {
                system("docker rmi -f \$(docker images -a | grep $choose | awk '{print \$3}')");
                printf("#\n#\n# Deleted successfully!\n#\n#");
            }
            else
            {
                printf("#\n#\n# Abort mission!\n#\n#");
            }
            exit;
        }
        printf("#\n# Unregcognized option \"$argv3\"!\n#\n");
        exit;
    }
    printf("#\n# Unregcognized option \"$argv2\"!\n#\n");
    exit;
}
elsif($argv1 eq 'container' || $argv1 eq 'c')
{
    if(!length($argv2)){ printf("# You must provide parameters!\n# \"$scriptname help\" for help"); exit; }
    if($argv2 eq 'show' || $argv2 eq 's')
    {
        system("docker container ls -a");
        exit;
    }
    elsif($argv2 eq 'del' || $argv2 eq 'd')
    {
        if(!length($argv3)){ printf("# You must provide parameters!\n# \"$scriptname help\" for help"); exit; }
        if($argv3 eq 'all' || $argv3 eq 'a')
        {
            clear();
            printf("# Do you want to delete all container?\n\n");
            system("docker container ls -a");
            printf("\n\n# (y/N): ");
            my $yN = <STDIN>;
            chomp($yN);
            if($yN eq 'y' || $yN eq 'Y')
            {
                system("docker rm -f \$(docker container ls -a | grep -v CONTAINER | awk '{print \$1}')");
                printf("#\n#\n# Deleted successfully!\n#\n#");
            }
            else
            {
                printf("#\n#\n# Abort mission!\n#\n#");
                exit;
            }
            exit;
        }
        elsif($argv3 eq 'byname' || $argv3 eq 'bn')
        {
            clear();
            system("docker container ls -a");
            printf("\n\n");
            printf("# Which container (by name) do you want to delete?\nContainer: ");
            my $choose = <STDIN>;
            chomp($choose);
            clear();
            printf("# Do you want to delete the following container?\n\n");
            system("docker container ls -a | grep $choose");
            printf("\n\n# (y/N): ");
            my $yN = <STDIN>;
            chomp($yN);
            if($yN eq 'y' || $yN eq 'Y')
            {
                system("docker rm -f \$(docker container ls -a | grep $choose | grep -v CONTAINER | awk '{print \$1}')");
                printf("#\n#\n# Deleted successfully!\n#\n#");
            }
            else
            {
                printf("#\n#\n# Abort mission!\n#\n#");
                exit;
            }
            exit;
        }
    }
    elsif($argv2 eq 'start')
    {
        if(!length($argv3)){ printf("# You must provide parameters!\n# \"$scriptname help\" for help"); exit; }
        if($argv3 eq 'all' || $argv3 eq 'a')
        {
            clear();
            printf("# NOTE: Broken containers can't be started!\n");
            printf("# Do you want to start all container?\n\n");
            system("docker ps -a | grep -v Up");
            printf("\n\n# (y/N): ");
            my $yN = <STDIN>;
            chomp($yN);
            if($yN eq 'y' || $yN eq 'Y')
            {
                system("docker start \$(docker ps -a | grep -v CONTAINER | grep -v Up | awk '{ print \$1 }')");
            }
            else
            {
                printf("#\n#\n# Abort mission!\n#\n#");
                exit;
            }
            exit;
        }
        elsif($argv3 eq 'byname' || $argv3 eq 'bn')
        {
            clear();
            system("docker container ls -a | grep -v Up");
            printf("\n\n");
            printf("# NOTE: Broken containers can't be started!\n");
            printf("# Which container (by name) do you want to start?\nContainer: ");
            my $choose = <STDIN>;
            chomp($choose);
            clear();
            printf("# Do you want to start the following container?\n\n");
            system("docker container ls -a | grep $choose");
            printf("\n\n# (y/N): ");
            my $yN = <STDIN>;
            chomp($yN);
            if($yN eq 'y' || $yN eq 'Y')
            {
                system("docker start \$(docker container ls -a | grep $choose | grep -v CONTAINER | awk '{print \$1}')");
                printf("#\n#\n# Started successfully!\n#\n#");
            }
            else
            {
                printf("#\n#\n# Abort mission!\n#\n#");
                exit;
            }
            exit;
        }
    }
    elsif($argv2 eq 'stop')
    {
        if(!length($argv3)){ printf("# You must provide parameters!\n# \"$scriptname help\" for help"); exit; }
        if($argv3 eq 'all' || $argv3 eq 'a')
        {
            clear();
            printf("# Do you want to stop all container?\n\n");
            system("docker ps -a | grep -v Exited");
            printf("\n\n# (y/N): ");
            my $yN = <STDIN>;
            chomp($yN);
            if($yN eq 'y' || $yN eq 'Y')
            {
                system("docker stop \$(docker ps -a | grep -v CONTAINER | grep -v Exited | awk '{ print \$1 }')");
            }
            else
            {
                printf("#\n#\n# Abort mission!\n#\n#");
                exit;
            }
            exit;
        }
        elsif($argv3 eq 'byname' || $argv3 eq 'bn')
        {
            clear();
            system("docker container ls -a | grep -v Exited");
            printf("\n\n");
            printf("# Which container (by name) do you want to stop?\nContainer: ");
            my $choose = <STDIN>;
            chomp($choose);
            clear();
            printf("# Do you want to stop the following container?\n\n");
            system("docker container ls -a | grep $choose");
            printf("\n\n# (y/N): ");
            my $yN = <STDIN>;
            chomp($yN);
            if($yN eq 'y' || $yN eq 'Y')
            {
                system("docker stop \$(docker container ls -a | grep $choose | grep -v CONTAINER | awk '{print \$1}')");
                printf("#\n#\n# Stopped successfully!\n#\n#");
            }
            else
            {
                printf("#\n#\n# Abort mission!\n#\n#");
                exit;
            }
            exit;
        }
        printf("#\n# Unregcognized option \"$argv3\"!\n#\n");
        exit;
    }
    printf("#\n# Unregcognized option \"$argv2\"!\n#\n");
    exit;
}
elsif($argv1 eq 'volumes' || $argv1 eq 'v')
{
    if(!length($argv2)){ printf("# You must provide parameters!\n# \"$scriptname help\" for help"); exit; }
    if($argv2 eq 'show' || $argv2 eq 's')
    {
        system("docker volume ls");
        exit;
    }
    elsif($argv2 eq 'del' || $argv2 eq 'd')
    {
        if(!length($argv3)){ printf("# You must provide parameters!\n# \"$scriptname help\" for help"); exit; }
        if($argv3 eq 'all' || $argv3 eq 'a')
        {
            clear();
            printf("# Do you want to delete all volumes?\n\n");
            system("docker volume ls");
            printf("\n\n# (y/N): ");
            my $yN = <STDIN>;
            chomp($yN);
            if($yN eq 'y' || $yN eq 'Y')
            {
                system("docker volume rm -f \$(docker volume ls | awk '{ print \$2 }')");
                printf("#\n#\n# Deleted successfully!\n#\n#");
            }
            else
            {
                printf("#\n#\n# Abort mission!\n#\n#");
                exit;
            }
            exit;
        }
        elsif($argv3 eq 'byname' || $argv3 eq 'bn')
        {
            clear();
            system("docker volume ls");
            printf("\n\n");
            printf("# Which volumes (by name) do you want to delete?\nVolume: ");
            my $choose = <STDIN>;
            chomp($choose);
            clear();
            printf("# Do you want to delete the following volumes?\n\n");
            system("docker volume ls | grep $choose");
            printf("\n\n# (y/N): ");
            my $yN = <STDIN>;
            chomp($yN);
            if($yN eq 'y' || $yN eq 'Y')
            {
                system("docker volume rm -f \$(docker volume ls | grep $choose | grep -v VOLUME | awk '{print \$2}')");
                printf("#\n#\n# Deleted successfully!\n#\n#");
            }
            else
            {
                printf("#\n#\n# Abort mission!\n#\n#");
                exit;
            }
            exit;
        }
    }
}
elsif($argv1 eq 'information' || $argv1 eq 'info')
{
    if(!length($argv2)){ printf("# You must provide parameters!\n# \"$scriptname help\" for help"); exit; }
    if($argv2 eq 'version' || $argv2 eq 'v')
    {
        printf("#\n#\n# ");
        system("docker -v");
        printf("#\n#\n");
        exit;
    }
    if($argv2 eq 'systeminfo' || $argv2 eq 'si')
    {
        clear();
        system("docker info");
        exit;
    }
    printf("#\n# Unregcognized option \"$argv2\"!\n#\n");
    exit;
}
elsif($argv1 eq 'help'){
    printhelp();
    exit;
}
else
{
    printf("#\n# Unregcognized option \"$argv1\"!\n#\n");
    exit;
}

# Subfunctions
sub clear
{
    system("clear");
}
sub printhelp
{
    system("clear");
    printf("#\n#\n#\n");
    printf("# Usage:\n");
    printf("# You can provide the following parameters to $scriptname:\n");
    printf("#\n");
    printf("# i|images:\n");
    printf("#       - show|s\n");
    printf("#       - del|d\n");
    printf("#           - all|a\n");
    printf("#           - byname|bn\n");
    printf("#\n");
    printf("# c|container:\n");
    printf("#       - show|s\n");
    printf("#       - del|d\n");
    printf("#           - all|a\n");
    printf("#           - byname|bn\n");
    printf("#       - start\n");
    printf("#           - all|a\n");
    printf("#           - byname|bn\n");
    printf("#       - stop\n");
    printf("#           - all|a\n");
    printf("#           - byname|bn\n");
    printf("#\n");
    printf("# v|volumes\n");
    printf("#       - show|s\n");
    printf("#       - del|d\n");
    printf("#           - all|a\n");
    printf("#           - byname|bn\n");
    printf("#\n");
    printf("# info|information:\n");
    printf("#       - version|v\n");
    printf("#       - systeminfo|si\n");
    printf("#\n");
    printf("# help\n");
    printf("#\n#\n#\n");
}
