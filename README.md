# Manage Docker Script

## Getting Started

Clone the repository in a accessible Directory.
Its a easy perl-script for managing docker.

### Prerequisites

The script is written in perl, so you need perl. :-)

```
For Windows i suggest Strawberry Perl http://strawberryperl.com/
```

### Installing

Clone the repository

```
git clone https://gitlab.com/ceed_/manage_docker.git
cd manage_docker && chmod +x manage_docker.pl
```

To make the script systemwide available, create e symbolic link

```
ln -s /path/to/manage_docker.pl /usr/local/bin/manage_docker
```

After this, the script should be executable with `manage_docker`


## Authors

* **Cedrik Heusser** - *Initial work*

## License

This project is licensed under the MIT License - see the [LICENSE.md](LICENSE.md) file for details

## Acknowledgments

* Hat tip to anyone whose code was used
